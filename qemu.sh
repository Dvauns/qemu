#!bin/bash

clear

echo "     __________"
echo "    |   ____   | EMU"
echo "    |  |    |  |"
echo "    |  |    |  |"
echo "    |  |____|  |"
echo "    |_______   |"
echo "            |  |"
echo "    V1-0    |__|"
echo ""
echo "(1) Start Qemu"
echo "(2) Start Qemu x86-64"
echo "(3) Settings"
echo "(4) About US"
echo "(X) Exit"
read doing

case $doing in
"1") bash $HOME/qemu/Data/start.sh
;;
"2") bash $HOME/qemu/Data/start2.sh
;;
"3") bash $HOME/qemu/Data/Setings.sh
;;
"4") bash $HOME/qemu/Data/about.sh
;;
"x") exit 0
;;
esac
