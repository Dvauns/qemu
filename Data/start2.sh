#!bin/bash


clear
echo "Qemu started"




qemu-system-x86_64 \
\
-m 912 \
\
-cdrom /storage/emulated/0/Documents/you file \
\
-m 7000M \
\
-cpu max \
\
-hda /storage/emulated/0/you file \
\
-vga std \
\
-net user \
\
-net nic,model=rtl8139 \
\
-usb -usbdevice tablet \
\
-accel tcg,thread=multi,tb-size=386 \
\
-smp 4,cores=4,sockets=1,threads=1 \
\
-M pc -monitor stdio -k en-us \
\
-device ac97 \
\
-vnc 127.0.0.1:2 









echo "Qemu Off"
