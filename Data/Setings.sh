#!bin/bash

clear

echo "     __________"
echo "    |   ____   | EMU"
echo "    |  |    |  |"
echo "    |  |    |  |"
echo "    |  |____|  |"
echo "    |_______   |"
echo "            |  |"
echo "    V1-0    |__|"
echo "SETINGS"
echo ""
echo "(1) Qemu setings"
echo "(2) Qemu x86-64 setings"
echo "(3) Create qcow2"
echo "(4) Create whd"
echo "(5) Create img"
read doing

case $doing in
"1") nano $HOME/qemu/Data/start.sh
;;
"2") nano $HOME/qemu/Data/start2.sh
;;
"3") qemu-img create -f qcow2 wm.qcow2 5g
;;
"4") qemu-img create -f qcow2 wm.whd 5g
;;
"5") qemu-img create -f qcow2 wm.img 5g
esac